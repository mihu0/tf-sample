resource "local_file" "foo" {
  content  = "foo: ${var.name}"
  filename = var.name
}

locals {
  name = "bar"
}

module "some" {
  source = "../tf-modules/component/"
  name = local.name
}

module "other" {
  source = "../tf-modules/component/"
  name = "other-${local.name}"
}

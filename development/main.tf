locals {
  name = "foo"
}

module "some" {
  source = "../tf-modules/component/"
  name = local.name
}
